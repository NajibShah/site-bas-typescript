# WECARE - CLIENT PORTAL

## Getting Started

To run the project locally you can follow the steps listed below:

1. Clone the project using `git clone` or download to a local folder.
2. Run `npm install`.
3. `npm start` to get the application to start after installation is complete.

### **WECARE-API**

To get the project to work properly you will need to clone/download the _wecare-api_ from [here](https://github.com/Tobivo-AB/wecare-api).

- ### **Connecting to the WECARE API**

  Once you have downloaded the the **wecare-api**, follow the same steps as listed above to get it running.
  i.e.

  1. `npm install` or `npm i`
  2. `npm start`

  Once it's running, the application will link up automatically.

  **NOTE:** You will need the wecare-api to be running in the background to get this project to work properly.

## Available Scripts For Development & Testing

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

[dev-unit]: https://github.com/Tobivo-AB/client-portal/workflows/unit%20tests/badge.svg
[dev-audit]: https://github.com/Tobivo-AB/client-portal/workflows/npm-audit/badge.svg
[dev-eslint]: https://github.com/Tobivo-AB/client-portal/workflows/eslint/badge.svg
[dev-build]: https://github.com/Tobivo-AB/client-portal/workflows/build/badge.svg
[mst-unit]: https://github.com/Tobivo-AB/client-portal/workflows/unit%20tests/badge.svg
[mst-audit]: https://github.com/Tobivo-AB/client-portal/workflows/npm-audit/badge.svg
[mst-eslint]: https://github.com/Tobivo-AB/client-portal/workflows/eslint/badge.svg
[mst-build]: https://github.com/Tobivo-AB/client-portal/workflows/build/badge.svg

|             |                                                     |
| ----------- | --------------------------------------------------- |
| **Develop** | ![dev-unit] ![dev-audit] ![dev-eslint] ![dev-build] |
| **Master**  | ![mst-unit] ![mst-audit] ![mst-eslint] ![mst-build] |
