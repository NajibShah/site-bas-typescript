import React from 'react';
import { render } from '@testing-library/react';
import { RegistrationForm } from './registration-form';
import { MemoryRouter } from 'react-router-dom';
import axios from 'axios';

describe('registration-form', () => {
  beforeAll(() => {
    jest.spyOn(axios, 'post').mockImplementation();
  });
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('renders registration-form correctly', () => {
    const { queryAllByLabelText } = render(<RegistrationForm />, {
      wrapper: MemoryRouter,
    });
    expect(queryAllByLabelText(/common/)).toHaveLength(6);
  });
});
