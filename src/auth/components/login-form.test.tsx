import React from 'react';
import { render } from '@testing-library/react';
import { LoginForm } from './login-form';
import { MemoryRouter } from 'react-router-dom';

describe('login-form', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  test('renders login-form correctly', () => {
    const { queryAllByLabelText } = render(<LoginForm />, {
      wrapper: MemoryRouter,
    });
    expect(queryAllByLabelText(/common/)).toHaveLength(2);
  });
});
