export type LoginState = {
  loggedIn: boolean;
  userId: string;
  token: string | null;
};
