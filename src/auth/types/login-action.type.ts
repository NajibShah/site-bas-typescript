export type LoginAction =
  | { type: 'login'; payload: { userId: string; token: string } }
  | { type: 'logout' };
