import { useContext } from 'react';
import { LoginContext } from '../contexts';

export function useLogin() {
  const {state, dispatch} = useContext(LoginContext);
  return [state, dispatch];
}
