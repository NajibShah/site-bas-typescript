import { AppNameItem } from '../common';

export const NavsConst :AppNameItem[] = [
  {label:'home', to: '/', key:1},
  {label:'about', to: '/about', key:2},
  {label:'users', to: '/users', key:3}
]
