import React from 'react';
import { render } from '@testing-library/react';
import { Header } from './header';
import { MemoryRouter } from 'react-router-dom';

test('renders Header component', () => {
  render(<Header />, { wrapper: MemoryRouter });
});
