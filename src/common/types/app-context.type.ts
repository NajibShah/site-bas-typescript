export type ApppContextType = {
  lang: string,
  setLang: (arg0: string) => void,
  loading: boolean
}