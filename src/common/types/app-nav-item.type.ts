export type AppNameItem = {
  label: string,
  to: string,
  key?: any
}